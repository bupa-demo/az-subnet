azure-terraform-modules

# Introduction 
Azure terraform module to create an Azure subnet and associate to NSG and/or Route.

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Provider
| Name    | Version |
| :---:   | :---:   |
| terraform | >=1.3.1 |
| azurerm | >=3.25.0 |

# Dependencies
This module depends on a new or existing resource group (refer to resource group module), network security group (refer to network security group module) and route table (refer to route table module)

# Usage
Reference the module as below and pass required and/or optional arguments to provision a network security group. This will create a single NSG and and additional rules defined.

```
module "subnet" {
  source                    = "git@ssh.dev.azure.com:v3/Delu2011/az-terraform-module/az-subnet"
  subnet_name               = var.subnet_name
  resource_group_name       = module.vnet_resource_group.resource_group_name
  virtual_network_name      = module.vnet.virtual_network_name
  subnet_cidr_list          = var.subnet_cidr_list
  network_security_group_id = module.nsg-v1.network_security_group_id
  route_table_id            = module.route-table.route_table_id
}
```

Example variables
```
###### Subnet ######
subnet_name           = "TrustNonProdSpoke-Subnet"
subnet_cidr_list      = ["10.0.5.0/24"]
```

# Inputs
| Name | Description | Type | Default | Required |
| :---: | :---: | :---: | :---: | :---: |
| subnet_name | Name of subnet to be created | string | n/a | yes |
| environment | Environment to deploy | string | n/a | no | 
| project | Name of project | string | n/a | no | 
| resource_group_name | Resource group name | string | n/a | yes |
| virtual_network_name | Virtual network name	 | string | n/a | yes |
| subnet_cidr_list | Address prefix list to use for the subnet | list(string) | n/a | yes |
| route_table_name | Route Table name to associate with the subnet | string | n/a | no |
| route_table_id | Route Table ID to associate with the subnet | string | n/a | no |
| network_security_group_name | Network Security Group name to associate with the subnets | string | n/a | no |
| network_security_group_id | Network Security Group ID to associate with the subnets | string | n/a | no |
| service_endpoints | Service endpoints to associate with the subnet | list(string) | n/a | no |
| service_endpoint_policy_ids | list of IDs of Service Endpoint Policies to associate with the subnet | list(string) | n/a | no |
| private_link_endpoint_enabled | Enable or disable network policies for the Private Endpoint on the subnet | bool | n/a | no |
| private_link_service_enabled | Enable or disable network policies for the Private Link Service on the subnet | bool | n/a | no |
| subnet_delegation | Configuration delegations on subnet | map(list(any)) | n/a | no |
| tags | Tags to add to resources | map(string) | n/a | no |


# Outputs
| Name | Description |
| :---: | :---: |
| subnet_cidr_list | CIDR list of the created subnets |
| subnet_id | Id of the created subnet |
| subnet_ips | The collection of IPs within this subnet |
| subnet_names | Names of the created subnet |

