variable "subnet_name" {
  description = "Subnet name"
  type        = string
}

variable "environment" {
  description = "Project environment"
  type        = string
  default     = ""
}

variable "project" {
  description = "Project name"
  type        = string
  default     = ""
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}

variable "virtual_network_name" {
  description = "Virtual network name"
  type        = string
}

variable "subnet_cidr_list" {
  description = "Address prefix list for subnet."
  type        = list(string)
}

variable "route_table_name" {
  description = "Route Table name to associate with the subnet."
  type        = string
  default     = null
}

variable "route_table_id" {
  description = "Route Table ID to associate with the subnet."
  type        = string
  default     = null
}

variable "network_security_group_name" {
  description = "Network Security Group name to associate with the subnets."
  type        = string
  default     = null
}

variable "network_security_group_id" {
  description = "Network Security Group ID to associate with the subnet."
  type        = string
  default     = null
}

variable "service_endpoints" {
  description = "List of Service endpoints to associate with the subnet."
  type        = list(string)
  default     = []
}

variable "service_endpoint_policy_ids" {
  description = "List of IDs of Service Endpoint Policies to associate with the subnet."
  type        = list(string)
  default     = null
}

variable "private_link_endpoint_enabled" {
  description = "Enable or disable network policies for the Private Endpoint on the subnet."
  type        = bool
  default     = null
}

variable "private_link_service_enabled" {
  description = "Enable or disable network policies for the Private Link Service on the subnet."
  type        = bool
  default     = null
}

variable "subnet_delegation" {
  description = <<EOD
Configuration delegations on subnet
object({
  name = object({
    name = string,
    actions = list(string)
  })
})
EOD
  type        = map(list(any))
  default     = {}
}
